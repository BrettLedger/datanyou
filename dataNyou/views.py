from django.shortcuts import render
from .models import File

def show_tree(request):
    return render(request, "tree.html", {'files': File.objects.all()})